
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "jedec.h"

/******************************* definitions *********************************/
#define GAL16V8 1 /* GAL types */
#define GAL20V8 2
#define GAL22V10 3
#define GAL20RA10 4
#define UNKNOWN 5
#define NOT_SPECIFIED 6

#define YES 1
#define NO 0

#define LOW 0
#define HIGH 1
#define INPUT_PIN 0
#define OUTPUT_PIN 1

/* GAL16V8 */
#define LOGIC16 0  /* location of the fuses */
#define XOR16 2048 /* in the JEDEC file     */
#define SIG16 2056
#define AC116 2120
#define PT16 2128
#define SYN16 2192
#define AC016 2193
#define NUMOFFUSES16 2194

/* GAL20V8 */
#define LOGIC20 0  /* location of the fuses */
#define XOR20 2560 /* in the JEDEC file     */
#define SIG20 2568
#define AC120 2632
#define PT20 2640
#define SYN20 2704
#define AC020 2705
#define NUMOFFUSES20 2706

/* GAL22V10 */
#define NUMOFFUSES22V10 5892 /* location of the fuses */
#define XOR22V10 5808        /* in the JEDEC file     */
#define SYN22V10 5809
#define SIG22V10 5828

/* GAL20RA10 */
#define NUMOFFUSES20RA10 3274 /* location of the fuses */
#define XOR20RA10 3200        /* in the JEDEC file     */
#define SIG20RA10 3210


#define LOGIC16_SIZE 2048 /* number of bits for XOR etc. */
#define LOGIC20_SIZE 2560
#define LOGIC22V10_SIZE 5808
#define LOGIC20RA10_SIZE 3200
#define ROW_SIZE_16V8 64
#define ROW_SIZE_20V8 64
#define ROW_SIZE_22V10 132
#define ROW_SIZE_20RA10 80
#define XOR_SIZE 8
#define SIG_SIZE 64
#define AC1_SIZE 8
#define PT_SIZE 64
#define SYN_SIZE 1
#define AC0_SIZE 1
#define ACW_SIZE 82 /* architecture control word (ACW) */

#define MAX_FUSE_ADR16 31 /* addresses of the GALs */
#define SIG_ADR16 32      /* (fuer Fan-Post :-))   */
#define MAX_FUSE_ADR20 39
#define SIG_ADR20 40
#define MAX_FUSE_ADR22V10 43
#define SIG_ADR22V10 44
#define MAX_FUSE_ADR20RA10 39
#define SIG_ADR20RA10 40
#define ACW_ADR 60
#define SECURITY_ADR 61
#define ERASE_ADR 63


/* output's polarity: */
#define ACTIVE_LOW 0  /* pin is high-active */
#define ACTIVE_HIGH 1 /* pin is low-active  */

/* type of the pin: */
#define NOTUSED 0     /* pin not used up to now */
#define NOTCON 0      /* pin not used           */
#define INPUT 2       /* input                  */
#define COMOUT 3      /* combinational output   */
#define TRIOUT 4      /* tristate output        */
#define REGOUT 5      /* register output        */
#define COM_TRI_OUT 6 /* either tristate or     */
                      /* combinational output   */

/* tristate control: */
#define NO_TRICON 0 /* no tristate control defined       */
#define TRICON 1    /* tristate control defined          */
#define TRI_VCC 2   /* permanent low  impedance          */
#define TRI_GND 3   /* permanent high impedance          */
#define TRI_PRO 4   /* tristate control via product term */


#define NC_PIN 30

#define MODE1 1 /* modes (SYN, AC0) */
#define MODE2 2
#define MODE3 3

#define MAX_SUFFIX_SIZE 6 /* max. string length of a legal suffix */

#define SIZE_OF_EQUASTRING 80


/******************************** structures *********************************/

/* used to store infos about a pin */
typedef struct {
    int8_t p_Neg; /* polarity of pin */
    int8_t p_Pin; /* pin number      */
} Pin_t;

/* used to store infos about an OLMC */
typedef struct {
    int8_t Active;   /* output's polarity           */
    int8_t PinType;  /* type of pin (input,...)     */
    int8_t TriCon;   /* user def. tristate control? */
    int8_t Clock;    /* user def. clock equation?   */
    int8_t ARST;     /* user def. ARST equation?    */
    int8_t APRST;    /* user def. APRST equation?   */
    int8_t FeedBack; /* is there a feedback?        */
} GAL_OLMC_t;

/*************************** function declartions ****************************/

int AssemblePldFile(const char* file, unsigned char* fbuff2, int size2, Config_t* cfg);

/* GALasm.c */
void WriteChipFile(char* filename, Config_t* cfg);
void WritePinFile(char* filename, Config_t* cfg);
void WriteSpaces(FILE* fp, int numof);
void WriteRow(FILE* fp, int row, int num_of_col);

void WriteFuseFile(char* filename, JedecStruct_t* jedec, Config_t* cfg);

